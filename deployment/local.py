import os
from defaults import DATABASES, PROJECT_PATH, ROOT_PATH

MEDIA_ROOT = os.path.join(ROOT_PATH, 'media')
STATIC_ROOT = os.path.join(PROJECT_PATH, 'collected_static')
STATIC_URL = '/static/'

TIME_ZONE = 'UTC'
CACHE_BACKEND = 'dummy:///'

DB = DATABASES['default']
DATABASE_NAME = DB['NAME']
DATABASE_USER = DB['USER']
DATABASE_PASSWORD = DB['PASSWORD']

MTURK_AUTH_EMAIL = 'abhishek@codemymobile.com'
MTURK_AUTH_PASSWORD = 'mahadev'

USE_CACHE = True
